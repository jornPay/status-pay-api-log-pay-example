# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

	// Define Obj
	$postObj = new stdClass;

	// http_code: die Pay.nl returned
	$postObj->http_code = 200;
	
	// url: De url die bij werd aangeroepen 
	$postObj->url = 'home/admin.pay.nl/logs/access_log';

	// method: Methode van de aanroep
	$postObj->method = 'POST';
	
	// useragent: De useragent die werd gebruikt om pay.nl
	// te bereiken
	$postObj->useragent = 'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0';

	// ip: Het ip van de enduser
	$postObj->ip = '213.126.82.230';

	// externalDate: Datum van aanroep
	$postObj->externalDate = 1479987715;


	
	// calls: amount => Amount of calls
	// calls: timing => response time in seconds
	$postObj->calls = [];

	$a = new stdClass;
	$a->amount = 1317;
	$a->timing = 0;

	$b = new stdClass;
	$b->amount = 1;
	$b->timing = 1;

	$c = new stdClass;
	$c->amount = 2;
	$c->timing = 3;

	$d = new stdClass;
	$d->amount = 1;
	$d->timing = 11;

	$postObj->calls = [];
	$postObj->calls[] = $a;
	$postObj->calls[] = $b;
	$postObj->calls[] = $c;
	$postObj->calls[] = $d;
	
	// Init CURL
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,            "http://status-pay.nl/api/log/pay" );
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt($ch, CURLOPT_POST,           true );
	curl_setopt($ch, CURLOPT_POSTFIELDS,     json_encode($postObj)); 
	curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain'));

	// Exec curl request
	$result=curl_exec ($ch);

	// Check for error
	if(curl_error($ch)){
		var_dump(curl_error($ch));
	}
	
	// Show Result
	var_dump($result);


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact